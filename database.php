<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;


$capsule = new Manager();

$capsule->addConnection([
	'driver'    => 'mysql',
	'host'      => 'localhost', # Хост
	'database'  => 'test', # Название БД
	'username'  => 'root', # Логин
	'password'  => 'test', # Пароль
	'charset'   => 'utf8',
	'collation' => 'utf8_unicode_ci', # Кодировка таблиц в БД
	'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();
$db = $capsule->connection();
var_dump($db);