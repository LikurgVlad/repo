<?php
require __DIR__ . '\vendor\autoload.php';

$url = explode('/', $_SERVER['REQUEST_URI']);

//class_alias('App\App', 'App');

$loader = new \Twig\Loader\FilesystemLoader('templates');
\App\ServiceLocator::set('twig', new \Twig\Environment($loader));
\App\ServiceLocator::set('database', new \App\Database());
\App\ServiceLocator::set('mailer', new \App\Mailer());


if(empty($url[1]))
	$obj = new \App\Controllers\Main();
elseif($url[1] == 'page')
	$obj = new \App\Controllers\Page();
else
	$obj = new \App\Controllers\Error404();
	
$obj->action_index();//sdfs