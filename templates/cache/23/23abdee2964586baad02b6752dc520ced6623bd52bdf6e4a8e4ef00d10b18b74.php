<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html */
class __TwigTemplate_0dbdc0cd4eb99df53a6d0f44066a2c785ee7e52c755b5a679f9d66697054cb59 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t</head>
\t<body>
\t\t<div id=\"header\">
\t\t</div>
\t\t<div id=\"menu\">
\t\t</div>
\t\t<div id=\"container\">
\t\t\t<div id=\"catalog\">
\t\t\t</div>
\t\t\t<div id=\"content\">
\t\t\t\t";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 16
        echo "\t\t\t</div>
\t\t</div>
\t\t<div id=\"footer\">
\t\t</div>
\t</body>
</html>";
    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  69 => 4,  60 => 16,  58 => 15,  44 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base.html", "E:\\OSPanel\\domains\\autoload\\templates\\base.html");
    }
}
