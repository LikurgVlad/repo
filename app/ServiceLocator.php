<?php
namespace App;

class ServiceLocator
{
	private static $services = [];
	
	public static function get($key)
	{
		return self::$services[$key];
	}
	public static function set($key, $value)
	{
		self::$services[$key] = $value;
	}
}