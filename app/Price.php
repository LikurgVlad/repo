<?php
namespace App;

use DiDom\Document;

class Price
{
	private $products;
	
	public function __construct()
	{
		$document = new Document('template.txt', true);
		$this->products = $document->find('product'); //Массив с элементами(экземпляры класса DiDom\Element) каждой позици.
	}
	
	private function getPrice($price_rub, $price_usd)
	{
		if(empty($price_rub))
			$product_price = $price_usd * 70;
		else
			$product_price = $price_rub;
		
		return $product_price;
	}
	
	public function getItemsArticles()
	{
		foreach($this->products as $product)
		{
			$article = $product->getAttribute('article');
			$price[] = $article;
		}
		return $price;
	}
	
	public function getItemsArticlesPrices()
	{
		foreach($this->products as $product)
		{
			$article = $product->getAttribute('article');
			
			// Работа с ценами
			$price_rub = (int)$product->getAttribute('price_rub');
			$price_usd = (int)$product->getAttribute('price_usd');
			
			$product_price = $this->getPrice($price_rub, $price_usd);
			
			$price[0][$article] = $product_price;
		}
		return $price;
	}
	
	public function getItems()
	{
		foreach($this->products as $product)
		{
			$article = $product->getAttribute('article');
			
			// Работа с ценами
			$price_rub = (float)$product->getAttribute('price_rub');
			$price_usd = (float)$product->getAttribute('price_usd');
			
			$product_price = $this->getPrice($price_rub, $price_usd);
			
			// Добовляем стоимость доставки
			if($product_price < 1200)
				$delivery_price = 300;
			elseif($product_price > 1199 && $product_price < 2000)
				$delivery_price = 200;
			else
				$delivery_price = 100;
			
			$category_name = $product->parent()->getAttribute('name'); // Название категории для текущего товара
			
			// Работа с картинками
			$images = $product->find('image'); // Массив с элементами(экземпляры класса DiDom\Element) каждой картинки текущей позиции.
			
			$images_urls = [];// Массив для url картинок текущей позици
			
			foreach($images as $img)
				$images_urls[] = $img->getAttribute('url');
			
			$product_name = $product->getAttribute('name');
			
			//Работа с количеством
			$quantity = $product->getAttribute('quantity');
			if($quantity < 10)
				$quantity_text = 'мало';
			else
				$quantity_text = 'много';
			
			
			$price[] = [
				'category'			=> $category_name,
				'article'			=> $article,
				'name'				=> $product_name,
				'price_rub'			=> $price_rub,
				'price_usd'			=> $price_usd,
				'final_price'		=> $product_price,	// Конвертированная(при необходимости) цена в рублях без доставки.
				'delivery_price'	=> $delivery_price,
				'quantity'			=> $quantity,
				'quantity_text'		=> $quantity_text,
				'imgs'				=> $images_urls
			];
		}
		return $price;
	}
}