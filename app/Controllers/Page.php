<?php
namespace App\Controllers;

class Page extends Controller
{
	public function action_index()
	{
		$content = 'page - контент страницы';
		
		echo $this->twig->render('page.twig', ['title' => 'page', 'style' => 'style.css', 'content' => $content]);
	}
}