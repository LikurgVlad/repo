<?php
namespace App\Controllers;

class Controller
{
	public function __get($name)
	{
		return \App\ServiceLocator::get($name);
	}
}