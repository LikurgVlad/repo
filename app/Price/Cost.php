<?php
namespace App\Price;

class Cost
{
	private $price_rub;
	private $price_usd;
	const DOLLAR_TO_RUBLE_REATE = 70;
	
	public function __construct(int $price_rub, float $price_usd)
	{
		$this->price_rub = $price_rub;
		$this->price_usd = $price_usd;
	}
	
	public function getPriceRub() : int {
		return $this->price_rub;
	}
		
	public function getPriceUsd() : float {
		return $this->price_usd;
	}
	
	public function getFinalPrice() : int
	{
		if(empty($this->price_rub))
			$final_price = $this->price_usd * self::DOLLAR_TO_RUBLE_REATE;
		else
			$final_price = $this->price_rub;
		
		return $final_price;
	}
}