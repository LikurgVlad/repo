<?php
namespace App\Price;

class Collection {
	
	private $items = [];
	
	public function addItem(Item $item) : void {
		$this->items[] = $item;
	}
	
	public function getItemsAll() : array {
		return $this->items;
	}
	
	public function getItemsArticles() : array
	{
		$price = [];
		foreach($this->items as $item)
			$price[] = $item->getArticle();
				
		return $price;
	}
	
	public function getItemsArticlesPrices() : array
	{
		$price = [];
		foreach($this->items as $item)
		{
			$article = $item->getArticle();
			$product_price = $item->getFinalPrice();
			
			$price[] = ['article' => $article, 'product_price' => $product_price];
		}
		return $price;
	}
}