<?php
namespace App\Price;

class Item
{
	private $category_name;
	private $article;
	private $product_name;
	private $images_urls = [];
	private $quantity;
	
	private $cost;
	
	public function __construct(Cost $cost, string $category_name,  string $article, string $product_name, int $quantity)
	{
		$this->cost = $cost;
		
		$this->category_name = $category_name;
		$this->article = $article;
		$this->product_name = $product_name;
		$this->quantity = $quantity;
	}
	
	public function addImageUrl(string $img) : void {
		$this->images_urls[] = $img;
	}
	
	public function getImagesUrls() : array {
		return $this->images_urls;
	}
	
	public function getQuantityText() : string
	{
		if($this->quantity < 10)
			$quantity_text = 'мало';
		else
			$quantity_text = 'много';
		
		return $quantity_text;
	}
	
	public function getFinalPrice() : int {
		return $this->cost->getFinalPrice();
	}
	
	public function getDeliveryPrice() : int
	{
		if($this->cost->getFinalPrice() < 1200)
			$delivery_price = 300;
		elseif($this->cost->getFinalPrice() < 2000)
			$delivery_price = 200;
		else
			$delivery_price = 100;
		
		return $delivery_price;
	}
	
	public function getCategoryName() : string {
		return $this->category_name;
	}
	
	public function getArticle() : string {
		return $this->article;
	}
	
	public function getProductName() : string {
		return $this->product_name;
	}
	
	public function getQuantity() : int {
		return $this->quantity;
	}
	
	public function getPriceRub() : int {
		return $this->cost->getPriceRub();
	}
	
	public function getPriceUsd() : float {
		return $this->cost->getPriceUsd();
	}
	
	/*public function getItem() : array
	{
		// Добавляем финальная цену в рублях
		//$final_price = $this->cost->getFinalPrice();
		
		// Добовляем стоимость доставки
		//$delivery_price = $this->getDelivery($final_price);
		
		// Добавляем текстовое обозначение количества $quantity
		//$quantity_text = $this->addQuantityText($this->quantity);
		
		$price_rub = $this->cost->getPriceRub();
		$price_usd = $this->cost->getPriceUsd();
		
		$price = [
				'category'			=> $this->category_name,
				'article'			=> $this->article,
				'name'				=> $this->product_name,
				'price_rub'			=> $price_rub,
				'price_usd'			=> $price_usd,
				'imgs'				=> $this->images_urls,
				'quantity'			=> $this->quantity,
				
				'final_price'		=> $final_price,	// Конвертированная(при необходимости) цена в рублях без доставки.
				'delivery_price'	=> $delivery_price,
				'quantity_text'		=> $quantity_text,
				
			];
		
		return $price;
	}*/
}