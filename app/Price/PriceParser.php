<?php
namespace App\Price;

use DiDom\Document;

class PriceParser
{
	public function getPrice() : Collection
	{
		$coll = new Collection();
		$document = new Document('template.txt', true); // ??? Лучше вернуть сразу $document?
		$products = $document->find('product');
		
		foreach($products as $product)
		{
			$category_name = $product->parent()->getAttribute('name'); // Название категории для текущего товара
			$article = $product->getAttribute('article');
			$product_name = $product->getAttribute('name');
			
			// Работа с ценами
			$price_rub = (int)$product->getAttribute('price_rub');
			$price_usd = (float)$product->getAttribute('price_usd');
			
			
			//Работа с количеством
			$quantity = $product->getAttribute('quantity');
			
			$cost = new Cost($price_rub, $price_usd);
			
			$item = new Item($cost, $category_name, $article, $product_name, $quantity);
			
			// Работа с картинками
			$images = $product->find('image'); // Массив с элементами(экземпляры класса DiDom\Element) каждой картинки текущей позиции.
			
			foreach($images as $img)
				$item->addImageUrl($img->getAttribute('url'));
			
			$coll->addItem($item);
		}
		
		return $coll;
	}
}